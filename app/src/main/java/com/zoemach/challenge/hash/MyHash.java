package com.zoemach.challenge.hash;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import androidx.annotation.NonNull;

public class MyHash extends IHash {

    private boolean gameover = false;

    private Paint paintPlayer = new Paint(Paint.ANTI_ALIAS_FLAG); // Used to paint the player representation
    private Paint paintGameOverPath = new Paint(Paint.ANTI_ALIAS_FLAG); // Red line on game over
    private Paint paintDefault = new Paint(Paint.ANTI_ALIAS_FLAG); // To paint a bitmap without modifications
    private Paint paintBackground = new Paint(Paint.ANTI_ALIAS_FLAG); // To paint the background bitmap
    private Paint paintHash = new Paint(Paint.ANTI_ALIAS_FLAG); // Used to paint lines of Hash


    public MyHash(int itemsSide, int players, int widthImage, int heightImage) {
        super(itemsSide, players, widthImage, heightImage);
        paintPlayer.setStyle(Paint.Style.STROKE);
        paintPlayer.setStrokeWidth(4);

        paintHash.setStyle(Paint.Style.STROKE);
        paintHash.setStrokeWidth(4);
        paintHash.setColor(Color.BLACK);

        paintGameOverPath.setStyle(Paint.Style.STROKE);
        paintGameOverPath.setStrokeWidth(4);
        paintGameOverPath.setColor(Color.RED);

        paintBackground.setAlpha(255/2);

        restartHash(itemsSide, players);
    }

    /** Return a player representation on bitmap with specific size (pixels)
     *
     * @param player Player id
     * @param size size of bitmap in pixels (width and height)
     * @return The bitmap representation of the specific player
     */
    @Override
    public Bitmap getPlayerRepresentation(int player, int size){

        //TODO Use the criativity to represent the player move
        // Tip: User "canvas" to paint on bitmap

        Bitmap representation = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(representation);


        return representation;
    }

    /** Return the representation of current player with specific size
     *
     * @param size size of bitmap (width and height)
     * @return
     */
    @Override
    public Bitmap getCurrentPlayerRepresentation(int size) {
        return getPlayerRepresentation(currentPlayer, size);
    }


    /**
     * @param bitmap Image for background
     */
    @Override
    public void setBackground(@NonNull Bitmap bitmap) {
        background = bitmap.copy(Bitmap.Config.ARGB_8888, true);
    }

    /**
     * Set the move of current player
     *
     * @param x xMouse (pixel) on hash image.
     * @param y yMouse (pixel) on hash image
     */
    @Override
    public void nextPlayerClick(int x, int y) {
        if(isGameOver()){
            return;
        }
        //TODO 1 - Register the player move; The user can click outside the image


        //TODO 2 - Check and register if the game is over;


        if(!isGameOver()) {
            //TODO 3 - Set the next player
        }
    }

    @Override
    public boolean isGameOver() {
        return gameover;
    }

    /**
     *
     * @return Hash Representation Bitmap with Background.
     * Will show "itemsSide" columns and rows with players plays
     * The Background bitmap opacity is 50%.
     * The background need to be resized to fill the hash image (without distortions). if necessary the background will be cropped
     * Remenber, the width and height of the image is not necessarily a square
     *
     */
    @Override
    public Bitmap getHashBitmap() {
        Bitmap resultHash = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvasHash = new Canvas(resultHash);

        //TODO Tip: User "canvas" to paint on bitmap. Use "Paint" objects to different kind of pencils
        //TODO 1 - Draw the background resized to fill the hash image (without distortions). if necessary the background will be cropped

        //TODO  2 - Draw the players representations on hash table

        //TODO  3 - Draw the Hash table (lines)

        if(isGameOver()) {
            // TODO 4 - Draw the red line on the winner row/col
        }

        return resultHash;
    }

    /** Restart the game
     * */
    @Override
    public void restartHash(int itemsSide, int players) {
        //TODO Restart the game with a new items size for each side (cols/rows) and new total players

        currentPlayer = 0;
    }
}
