package com.zoemach.challenge.hash;

import android.graphics.Bitmap;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;


public abstract class IHash {

    protected int itemsSide;
    protected int width, height = 0;
    protected Bitmap background, hash;
    protected int totalPlayers = 2;
    protected int currentPlayer = 0;

    /**
     * Constructor of hash.
     *
     * @param itemsSide Number of items from side. Ex.: 3x3, 5x5, etc
     * @param players number of players
     * @param widthImage Width of hash image
     * @param heightImage Height of hash image
     */
    public IHash(@IntRange(from = 3, to = 10) int itemsSide,
                 @IntRange(from = 2, to = 5) int players,
                 @IntRange(from = 300, to = 1000) int widthImage,
                 @IntRange(from = 300, to = 1000) int heightImage){

        this.width = widthImage;
        this.height = heightImage;
        this.totalPlayers = players;
        this.itemsSide = itemsSide;

    }

    public void setWidth(@IntRange(from = 300, to = 1000) int width) {
        this.width = width;
    }

    public void setHeight(@IntRange(from = 300, to = 1000) int height) {
        this.height = height;
    }

    public int getTotalPlayers() {
        return totalPlayers;
    }

    public int getCurrentPlayerId() {
        return currentPlayer + 1;
    }

    /**
     * Set the move of current player
     *
     * @param x xMouse (pixel) on hash image
     * @param y yMouse (pixel) on hash image
     */
    public abstract void nextPlayerClick(int x, int y);


    /**
     *
     * @return If the game is over
     */
    public abstract boolean isGameOver();


    /** Set the background of hash.
     * @param bitmap
     */
    public abstract void setBackground(@NonNull Bitmap bitmap);


    /**
     *
     * @return Hash Representation with Background. The bitmap opacity is 50%.
     * The background need to be resized to fill the hash image (without distortions). if necessary the background will be cropped
     *
     */
    public abstract Bitmap getHashBitmap();

    /**
     * @param itemsSide Number of items from side. Ex.: 3x3, 5x5, etc
     * @param players number of players
     *
     * Restart Game
     *
     */
    public abstract void restartHash(@IntRange(from = 3, to = 10) int itemsSide,
                              @IntRange(from = 2, to = 5) int players);

    /**
     * @param player Player id
     * @param size size of bitmap (width and height)
     * @return The bitmap representation of the specific player (USE THE CRIATIVITY TO REPRESENT THE PLAYER)
     */
    public abstract Bitmap getPlayerRepresentation(int player, int size);

    /**
     * @param size size of bitmap (width and height)
     * @return The bitmap representation of the current player (USE THE CRIATIVITY TO REPRESENT THE PLAYER)
     */
    public abstract Bitmap getCurrentPlayerRepresentation(int size);

}
