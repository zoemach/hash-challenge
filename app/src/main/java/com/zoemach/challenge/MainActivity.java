package com.zoemach.challenge;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoemach.challenge.hash.IHash;
import com.zoemach.challenge.hash.MyHash;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {
    private static final String TAG = "HashActivity";

    private IHash myHash = new MyHash(3, 2, 500, 800);

    @BindView(R.id.ivCurrentPlayer)
    ImageView ivCurrentPlayer;

    @BindView(R.id.ivHash)
    ImageView ivHash;

    @BindView(R.id.titleNextPlayer)
    TextView titleNextPlayer;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ivHash.setOnTouchListener(this);

        drawHash();

    }

    @OnClick({R.id.ivImage1, R.id.ivImage2, R.id.ivImage3, R.id.ivImage4})
    public void onClickImage(View imageView){
        Log.i(TAG, "Background Clicked");
        Bitmap background;
        switch (imageView.getId()){
            case R.id.ivImage1:
                background = BitmapFactory.decodeResource(getResources(), R.drawable.image1);
                break;
            case R.id.ivImage2:
                background = BitmapFactory.decodeResource(getResources(), R.drawable.image2);
                break;
            case R.id.ivImage3:
                background = BitmapFactory.decodeResource(getResources(), R.drawable.image3);
                break;
            case R.id.ivImage4:
                background = BitmapFactory.decodeResource(getResources(), R.drawable.image4);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + imageView.getId());
        }
        myHash.setBackground(background);

        drawHash();
    }

    @OnClick(R.id.btReset)
    public void onClickReset(View view){
        Log.i(TAG, "Reset Clicked");
        myHash.restartHash(3 + (int)(Math.random()*5), 2 + (int)(Math.random()*3));
        myHash.setWidth(300 + (int)(Math.random()*700));
        myHash.setHeight(300 + (int)(Math.random()*700));
        drawHash();
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        final int index = event.getActionIndex();
        final float[] coords = new float[]{event.getX(index), event.getY(index)};
        Matrix m = new Matrix();
        ivHash.getImageMatrix().invert(m);
        m.postTranslate(ivHash.getScrollX(), ivHash.getScrollY());
        m.mapPoints(coords);

        int xOnHash = (int) coords[0];
        int yOnHash = (int) coords[1];

        Log.i(TAG, "Coord("+xOnHash+", "+yOnHash+")");

        myHash.nextPlayerClick(xOnHash, yOnHash);
        drawHash();

        return false;
    }

    private void drawHash(){
        Log.i(TAG, "Hash Updated");
        titleNextPlayer.setText("Next Player ("+myHash.getCurrentPlayerId()+"/"+myHash.getTotalPlayers()+")");
        ivHash.setImageBitmap(myHash.getHashBitmap());
        ivCurrentPlayer.setImageBitmap(myHash.getCurrentPlayerRepresentation(80));
    }
}