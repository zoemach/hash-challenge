# HASH (TIC TAC TOE) CHALLENGE

The challenge is to implement the functions that are pending to be completed for the execution of the modified Hash (or tic tac toe) game. The game has a simple interface and functions.

The challenger only needs to complete the methods of the class *"com.zoemach.challenge.hash.MyHash"*, all pending methods have **TODO** reminders.

## Requirements and Rules

1. The App must allow selecting the game's background image. The image list is now available on the interface. The background needs to be centered and resized to fill the game image without distortion. If necessary, the background can be cut.
2. When restarting the game, the app will choose at random the width and height of the game (pixels), the number of sides (maximum 8) and players (maximum 5); Each player has a representation. In the classic game, the 2 players are represented by "X" and "O", use your creativity to represent the others;
3. The App must display the image that represents the next player to make a move;
4. When clicking on the game image the app must register the movement, check if the game is over and move on to the next player;
5. Whoever completes a complete row or column wins. There is no need to consider the diagonal.

## Examples

![Hash1](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash1.jpeg)
![Hash2](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash2.jpeg)
![Hash3](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash3.jpeg)
![Hash4](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash4.jpeg)
![Hash5](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash5.jpeg)
![Hash6](https://bitbucket.org/zoemach/hash-challenge/downloads/Hash6.jpeg)
